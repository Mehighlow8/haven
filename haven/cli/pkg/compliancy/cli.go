// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	cli "github.com/jawher/mow.cli"
	"gitlab.com/commonground/haven/haven/cli/pkg/logging"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var (
	kubeLatest = "undefined"
)

// CLIConfig renders the command line function
var CLIConfig = func(config *cli.Cmd) {
	logFile := config.StringOpt("log-file", "", "Enables writing output logs to file by specifying the path. Optional.")
	sshHost := config.StringOpt("ssh-host", "", "[Compliancy] SSH host name of a master node. Required, results in skipped tests when omitted.")
	runCNCFChecks := config.BoolOpt("cncf", true, "[Compliancy] Run external CNCF checks. Required, results in skipped test when set to false.")
	runCISChecks := config.BoolOpt("cis", false, "[Suggestion] Run external CIS checks by specifying log output file path. Optional.")

	config.Action = func() {
		cmdCheck(logFile, sshHost, runCNCFChecks, runCISChecks)
	}
}

var (
	sshConfigured  bool = false
	cncfConfigured bool = false
)

// cmdChecks validates requirements, runs the in scope checks and outputs
// results.
func cmdCheck(logFile *string, sshHost *string, runCNCFChecks *bool, runCISChecks *bool) {
	logging.LogFile = *logFile

	logging.Debug("HAVEN COMPLIANCY CHECKER - STARTED\n")
	defer logging.Debug("HAVEN COMPLIANCY CHECKER - FINISHED\n\n\n")

	kubeConfig, err := connectK8s()
	if err != nil {
		logging.Fatal("Could not connect to Kubernetes: %s\n", err.Error())
		return
	}

	if "" != *sshHost {
		err := connectSsh(sshHost)
		if err != nil {
			logging.Fatal("SSH: Could not connect to host: %s\n", err.Error())
			return
		}

		sshConfigured = true
		logging.Info("SSH: Configured host '%s'. Enabled checks requiring SSH access.\n", *sshHost)
	}

	if !sshConfigured {
		logging.Warning("SSH: Not configured! Skipping compliancy checks requiring SSH access.\n")
	}

	if *runCNCFChecks {
		if path, err := exec.LookPath("sonobuoy"); err == nil {
			cncfConfigured = true

			logging.Info("CNCF: Sonobuoy binary found at '%s'. Enabled external CNCF compliancy check.\n", path)
		} else {
			logging.Warning("CNCF: Sonobuoy binary not found! Skipping external CNCF compliancy check.\n")
		}
	} else {
		logging.Warning("CNCF: Opted out! Skipping external CNCF compliancy check.\n")
	}

	if *runCISChecks {
		logging.Info("CIS: Opted in. Enabled external CIS suggested check.\n")
	} else {
		logging.Info("CIS: Not opted in. Skipping external CIS suggested check.\n")
	}

	logging.Info("Running checks...")

	checker, err := NewChecker(kubeConfig, kubeLatest, *sshHost, cncfConfigured, *runCISChecks)
	if err != nil {
		logging.Fatal("Could not start checker: %s\n", err.Error())
		return
	}

	compliancyResults, suggestedResults, err := checker.Run()
	if err != nil {
		logging.Error("Checker encountered an error: %s\n", err.Error())
		return
	}

	checker.PrintResults(CompliancyChecks, compliancyResults, true)
	checker.PrintResults(SuggestedChecks, suggestedResults, false)
}

// connectK8s attempts to connect to a Kubernetes cluster via kube config or
// in cluster.
func connectK8s() (*rest.Config, error) {
	var cfg *rest.Config
	var err error
	var path string

	path = os.Getenv("KUBECONFIG")
	if "" == path {
		var home = os.Getenv("HOME")
		if "" == home {
			home = os.Getenv("USERPROFILE")
		}

		path = filepath.Join(home, ".kube", "config")
	}

	if _, err = os.Stat(path); err == nil {
		cfg, err = clientcmd.BuildConfigFromFlags("", path)
		if err != nil {
			logging.Error("Error while parsing KUBECONFIG: %s.\n", err)
			return nil, err
		}

		logging.Info("Kubernetes connection using KUBECONFIG: %s.\n", path)
	}

	if nil == cfg {
		cfg, err = rest.InClusterConfig()
		if err != nil {
			return nil, err
		}

		logging.Info("Kubernetes connection using In Cluster config.\n")
	}

	return cfg, nil
}

// connectSsh attempts to connect to the given node and assert this works.
func connectSsh(sshHost *string) error {
	c := strings.Split(fmt.Sprintf("ssh %s ''%s''", *sshHost, "uname -a"), " ")
	cmd := exec.Command(c[0], c[1:]...)

	cfg := `
ForwardAgent no
ServerAliveInterval 120

Host bastion
  User core
  IdentityFile ~/tmp/ssh_id_rsa
  Hostname 11.22.33.44

Host 10.0.101.*
  User core
  IdentityFile ~/tmp/ssh_id_rsa
  ProxyJump bastion
				`

	if err := cmd.Run(); err != nil {
		return errors.New(fmt.Sprintf("SSH: configured host '%s', but connection failed: '%s'\n\nTry to configure ~/.ssh/config like this:\n%s\n\n", *sshHost, err.Error(), cfg))
	}

	return nil
}
