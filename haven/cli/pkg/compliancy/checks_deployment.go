// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"strings"

	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func deploymentLogAggregation(config *Config) (Result, error) {
	loggers := []string{
		"promtail",
		"fluentd",
		"logstash",
		"filebeat",
		"splunk",
		"logentries",
		"logspout", // One of the recommended papertrail k8s deployments.
		"logagent",
		"collectd",
		"statsd",
		"syslog-ng",
		"event-exporter-gke", // GKE.
	}

	pods, err := config.KubeClient.CoreV1().Pods(apiv1.NamespaceAll).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, p := range pods.Items {
		for _, l := range loggers {
			if strings.Contains(p.Name, l) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

func deploymentMetricsServer(config *Config) (Result, error) {
	metrics := []string{
		"metrics-server",
		"prometheus",
	}

	pods, err := config.KubeClient.CoreV1().Pods(apiv1.NamespaceAll).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, p := range pods.Items {
		for _, l := range metrics {
			if strings.Contains(p.Name, l) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

func deploymentHttpsCerts(config *Config) (Result, error) {
	// Detection via Pods.
	providerPods := []string{
		"traefik",
		"cert-manager",
		"voyager",             // HAProxy with LetsEncrypt.
		"service-ca-operator", // OpenShift.
	}

	pods, err := config.KubeClient.CoreV1().Pods(apiv1.NamespaceAll).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, pod := range pods.Items {
		for _, l := range providerPods {
			if strings.Contains(pod.Name, l) {
				return ResultYes, nil
			}
		}
	}

	// Detection via CRD's.
	providerCrds := []string{
		"ManagedCertificate", // GKE.
	}

	crds, err := config.ExtensionsClient.ApiextensionsV1().CustomResourceDefinitions().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return ResultNo, err
	}

	for _, crd := range crds.Items {
		for _, l := range providerCrds {
			if strings.Contains(crd.Spec.Names.Kind, l) {
				return ResultYes, nil
			}
		}
	}

	return ResultNo, nil
}

// suggestionDeploymentHavenDashboard is a 'Suggested Check' meaning it's not counted towards Haven Compliancy.
func suggestionDeploymentHavenDashboard(config *Config) (Result, error) {
	list, err := config.KubeClient.AppsV1().Deployments(apiv1.NamespaceAll).List(context.Background(), metav1.ListOptions{})

	if err != nil {
		return ResultNo, err
	}

	for _, d := range list.Items {
		if "haven-dashboard" == d.Name {
			return ResultYes, nil
		}
	}

	return ResultNo, nil
}
