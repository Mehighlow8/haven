// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package main

import (
	"os"

	"github.com/gookit/color"
	cli "github.com/jawher/mow.cli"
	"gitlab.com/commonground/haven/haven/cli/pkg/addons"
	"gitlab.com/commonground/haven/haven/cli/pkg/compliancy"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
)

var (
	version = "v5.1.0"
)

func main() {
	color.Bold.Printf("Haven %s - Copyright © VNG Realisatie 2019-2021 - Licensed under the EUPL v1.2.\n\n", version)

	app := cli.App("haven", "")

	app.Command("check", "Runs compliancy checks", compliancy.CLIConfig)
	app.Command("addons", "Manage addons on a cluster", addons.CLIConfig)

	app.Run(os.Args)
}
