# Haven CLI

The Haven CLI can be used to validate an existing cluster on Haven Compliancy and to manage installation of addons.

## Usage
See `haven --help`.

### Compliancy Checker
Haven Compliancy Checker runs against existing Kubernetes clusters, checking for Haven Compliancy by executing multiple checks.

See `haven check --help`.

- Enable output logging to file with --log-file <path>. Output includes debug information and CIS results when enabled.
- Opt in to checks requiring SSH access with --ssh-host <ip-address-of-any-master-node>. *SSH checks are required for Haven Compliancy*.
- Opt out of external CNCF checks with `haven check --cncf=false`. *CNCF checks are required for Haven Compliancy*.
- Opt in to external CIS benchmark checks with `haven check --cis`. CIS checks are optional and not required for Haven Compliancy.

*Haven Compliancy thus requires your final command to end up like this: `haven check --ssh-host 172.31.3.37`*.

(And the full options explicit version could look like this: `haven check --ssh-host 10.0.0.42 --cncf --cis --log-file ~/hcc.log`.)

### Addons
Addons are an easy but optional way to deploy basics like logging on your Haven cluster, by using the Haven CLI.

See `haven addons --help`.

For example installing the monitoring addon can be done using: `haven addons install monitoring`.
The Haven CLI will interactively guide the user through required configuration parameters like providing a hostname.

## License
Copyright © VNG Realisatie 2019-2021
Licensed under the EUPL
