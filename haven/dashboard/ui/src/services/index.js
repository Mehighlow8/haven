import { ReleasesService } from './releases'
import { NamespacesService } from './namespaces'
import { OIDCService } from './oidc'

const services = {
    releases: new ReleasesService(),
    namespaces: new NamespacesService(),
    oidc: new OIDCService(),
}

export default services
