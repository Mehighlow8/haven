import styled from 'styled-components/macro'

const Section = styled.section`
  margin-top: ${(p) => p.theme.tokens.spacing08};
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
`

export default Section
