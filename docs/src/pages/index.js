import React from 'react'

import Layout from '../components/Layout'
import Flex from '../components/Flex'
import Box from '../components/Box'
import SEO from '../components/SEO'
import Container from '../components/Container'
import Jumbotron from '../components/Jumbotron'
import Section from '../components/Section'
import ResponsiveImage from '../components/ResponsiveImage'

import compliancyChecker from '../images/screenshots/compliancy-checker.png'
import referenceOpenStack from '../images/screenshots/reference-openstack.png'
import monitoring from '../images/screenshots/monitoring.png'

const IndexPage = () => (
  <Layout>
    <SEO />
    <Jumbotron>
      <h1>Haven</h1>
      <p>Standaard voor cloud-agnostische infrastructuur</p>
    </Jumbotron>
    <Container>
      <Section>
        <Flex scrollable>
          <Box width={1/3}>
            <h2>Uniform</h2>
            <p>Een applicatie die werkt op &eacute;&eacute;n Haven omgeving werkt op &aacute;lle Haven omgevingen.</p>
            <p>Ongeacht onderliggende techniek zoals cloud en on-premise oplossingen.</p>
          </Box>
          <Box width={1/3}>
            <h2>Herbruikbaar</h2>
            <p>Alles is open source inclusief de hulpmiddelen en referentie implementaties.</p>
            <p>Hiermee kunnen overheden en leveranciers eenvoudig hun eigen Haven omgevingen opzetten.</p>
          </Box>
          <Box width={1/3}>
            <h2>Veilig</h2>
            <p>Een Haven omgeving is standaard geconfigureerd voor veiligheid.</p>
            <p>Onder meer gebruik van een afgescheiden netwerk en logging zijn vereist.</p>
          </Box>
        </Flex>
      </Section>

      <hr />

      <Section>
        <Flex>
          <Box width={2/5}>
            <h2>Geautomatiseerde validatie</h2>
            <p>
              Met de Haven Compliancy Checker kunnen clusters geautomatiseerd valideren op de standaard. Dit brengt duidelijkheid en transparantie met zich mee.
            </p>
          </Box>
          <Box width={3/5}>
            <ResponsiveImage src={compliancyChecker} alt="Schermafbeelding van de Haven Compliancy Checker" />
          </Box>
        </Flex>
      </Section>

      <Section>
        <Flex>
          <Box width={2/5}>
            <h2>Inzicht en eenvoud</h2>
            <p>
              Addons zoals Monitoring en het Haven Dashboard bieden inzicht in de prestaties van het cluster inclusief applicaties en maken het beheer eenvoudiger.
            </p>
          </Box>
          <Box width={3/5}>
            <ResponsiveImage src={monitoring} alt="Schermafbeelding van grafieken die de prestatie van een cluster tonen" />
          </Box>
        </Flex>
      </Section>

      <Section>
        <Flex>
          <Box width={2/5}>
            <h2>Referentie Implementaties</h2>
            <p>
              Gemeenten en leveranciers zijn vrij om naar eigen inzicht een Haven omgeving in te richten.
            </p>
            <p>
              Een groeiend aantal Referentie Implementaties kunnen hierbij ondersteunen, denk bijvoorbeeld aan OpenStack (zie voorbeeld), Azure en Amazon.
            </p>
          </Box>
          <Box width={3/5}>
            <ResponsiveImage src={referenceOpenStack} alt="Schermafbeelding van de OpenStack Referentie Implementatie" />
          </Box>
        </Flex>
      </Section>

    </Container>
  </Layout>
)

export default IndexPage
