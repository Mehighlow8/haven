---
title: "Databases"
path: "/docs/databases"
---

Haven biedt de mogelijkheid om geautomatiseerd databases aan te maken. We gebruiken in deze toelichting hiervoor twee verschillende operators: [KubeDB](https://kubedb.com/) en de [Postgres Operator](https://postgres-operator.readthedocs.io/). De operators automatiseren verschillende beheertaken waaronder het maken van backups, monitoring en failover.

## KubeDB

KubeDB beheert Elasticsearch, Memcached, MongoDB, MySQL, PostgreSQL en Redis databases.

Aan de hand van het volgende voorbeeld kun je eenvoudig een MySQL database aanmaken.

We kijken eerst welke versies er op dit moment beschikbaar zijn met:

```bash
$ kubectl get mysqlversions
NAME     VERSION   DB_IMAGE              DEPRECATED   AGE
5        5         kubedb/mysql:5        true         57d
5-v1     5         kubedb/mysql:5-v1     true         57d
5.7      5.7       kubedb/mysql:5.7      true         57d
5.7-v1   5.7       kubedb/mysql:5.7-v1   true         57d
5.7-v2   5.7.25    kubedb/mysql:5.7-v2                57d
5.7.25   5.7.25    kubedb/mysql:5.7.25                57d
8        8         kubedb/mysql:8        true         57d
8-v1     8         kubedb/mysql:8-v1     true         57d
8.0      8.0       kubedb/mysql:8.0      true         57d
8.0-v1   8.0.3     kubedb/mysql:8.0-v1                57d
8.0-v2   8.0.14    kubedb/mysql:8.0-v2                57d
8.0.14   8.0.14    kubedb/mysql:8.0.14                57d
8.0.3    8.0.3     kubedb/mysql:8.0.3                 57d
```

Versies waar het veld *deprecated* op true staat kunnen niet aangemaakt worden. Maak voor een eenvoudige database het volgende bestand:

*eerste-database-mysql.yaml*
```yaml
apiVersion: kubedb.com/v1alpha1
kind: MySQL
metadata:
  name: eerste-database
  namespace: default
spec:
  version: "8.0.14"
  storageClassName: cinder
  storageType: Durable
  storage:
    accessModes:
      - ReadWriteOnce
    resources:
      requests:
        storage: 1Gi
```

Hier specificeer je de versie van MySQL en de grootte van de onderliggende storage. Maak de database vervolgens aan met:

```bash
$ kubectl apply -f eerste-database-mysql.yaml
```

De status van de database kan nu opgehaald worden met:

```bash
$ kubectl get mysql
NAME            VERSION   STATUS     AGE
eerste-database 8.0.14    Creating   8s
```

Na een minuut is de database beschikbaar:

```bash
NAME            VERSION   STATUS    AGE
eerste-database 8.0.14    Running   44s
```

KubeDB heeft zelf een service en een secret aangemaakt voor deze database:

```bash
$ kubectl get service,secret
NAME                        TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/eerste-database     ClusterIP   100.68.141.225   <none>        3306/TCP   57s
service/eerste-database-gvr ClusterIP   None             <none>        3306/TCP   58s

NAME                                                              TYPE                                  DATA   AGE
secret/eerste-database-auth                                       Opaque                                2      57s
secret/eerste-database-snapshot-token-5hkv8                       kubernetes.io/service-account-token   3      58s
secret/eerste-database-token-49dp2                                kubernetes.io/service-account-token   3      58s
```

Een applicatie kan nu verbinding maken met deze database door te verbinden met de host *eerste-database*. De inloggegevens voor de database zijn automatisch gegenereerd door KubeDB en te vinden in de secret *eerste-database-auth*.

Meer informatie over onder andere het gebruik van andere databases en backups kun je vinden in de [handleiding van KubeDB](https://kubedb.com/docs/v0.13.0-rc.0/guides/).


## Postgres Operator

De Postgres Operator is gespecialiseerd in het beheren van Postgres databases en wordt aangeraden wanneer je een Postgres database of cluster wil uitrollen. Maak voor een eenvoudige database het volgende bestand:

*eerste-database-postgresql.yaml*
```yaml
apiVersion: "acid.zalan.do/v1"
kind: postgresql
metadata:
  name: eerste-database
spec:
  teamId: "acid"
  volume:
    size: 1Gi
  numberOfInstances: 2
  users:
    # database eigenaar
    zalando:
      - superuser
      - createdb
  #databases: naam->eigenaar
  databases:
    foo: zalando
  postgresql:
    version: "12"
```

Maak vervolgens de database aan met:

```bash
$ kubectl apply -f eerste-database-postgresql.yaml
```

Bekijk de status van de database met:

```bash
$ kubectl get postgresql
NAME                   TEAM   VERSION   PODS   VOLUME   CPU-REQUEST   MEMORY-REQUEST   AGE   STATUS
acid-eerste-database   acid   12        2      1Gi                                     49s   Creating
```

Na ongeveer een minuut is de database beschikbaar:

```bash
$ kubectl get postgresql
NAME                   TEAM   VERSION   PODS   VOLUME   CPU-REQUEST   MEMORY-REQUEST   AGE   STATUS
acid-eerste-database   acid   12        2      1Gi                                     92s   Running
```

De Postgres Operator heeft verschillende services en secrets aangemaakt:

```bash
$ kubectl get service,secret
NAME                                  TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/acid-eerste-database          ClusterIP   100.64.25.253    <none>        5432/TCP   2m47s
service/acid-eerste-database-config   ClusterIP   None             <none>        <none>     2m19s
service/acid-eerste-database-repl     ClusterIP   100.65.114.120   <none>        5432/TCP   2m47s

NAME                                                                        TYPE                                  DATA   AGE
secret/postgres.acid-eerste-database.credentials.postgresql.acid.zalan.do   Opaque                                2      2m47s
secret/standby.acid-eerste-database.credentials.postgresql.acid.zalan.do    Opaque                                2      2m47s
secret/zalando.acid-eerste-database.credentials.postgresql.acid.zalan.do    Opaque                                2      2m47s
```

Je kunt verbinden met de database via de hostname *acid-eerste-database*. De inloggegevens zijn te vinden in de secret *postgres.acid-eerste-database.credentials.postgresql.acid.zalan.do*.

Meer informatie over de Postgres Operator kun je vinden in de [handleiding](https://postgres-operator.readthedocs.io/en/latest/user/).
