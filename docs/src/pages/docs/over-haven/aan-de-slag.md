---
title: "Aan de slag"
path: "/docs/aan-de-slag"
---

Concreet kunnen gemeenten en leveranciers zelf een omgeving inrichten bij een cloudprovider of on-premise. Vervolgens kan deze nieuwe omgeving worden getoetst met de Haven Compliancy Checker.

## Referentie Implementaties

Een groeiend aantal Referentie Implementaties maken het eenvoudiger om met Haven aan de slag te gaan. Gebruik is dus niet verplicht. Ze zijn te downloaden op [GitLab](https://gitlab.com/commonground/haven/haven/reference) en wij moedigen aan om nieuwe Referentie Implementaties aan te bieden via een Merge Request.

De [OpenStack](https://gitlab.com/commonground/haven/haven/reference) versie wordt gebruikt om verschillende teams bij VNG Realisatie te voorzien van een productie-waardig cluster.

## Voorbeelden

Een aantal mogelijke cloudoplossingen:

- [OpenStack](https://www.openstack.org/) (Kops)- [**Aan de slag**](/docs/aan-de-slag/openstack)
- [Amazon Elastic Kubernetes Service](https://aws.amazon.com/eks/) (EKS) - [**Aan de slag**](/docs/aan-de-slag/aws)
- [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine) (GKE)
- [IBM Cloud Kubernetes Service](https://www.ibm.com/cloud/container-service/)
- [Microsoft Azure Kubernetes Service](https://azure.microsoft.com/nl-nl/services/kubernetes-service/) (AKS) - [**Aan de slag**](/docs/aan-de-slag/azure)

En een aantal mogelijke on-premise oplossingen:

- [Red Hat OpenShift](https://www.openshift.com/)
- [VMware Tanzu Kubernetes Grid](https://tanzu.vmware.com/kubernetes-grid)

De benoeming van een mogelijke oplossing op deze pagina betekent dat we hebben geconstateerd dat het mogelijk is om er een Haven Compliant omgeving op in te richten. We spreken er nadrukkelijk geen voorkeuren en/of aanbevelingen mee uit. Deze lijst is niet volledig. Het indienen van een [Merge Request](https://gitlab.com/commonground/haven/haven/-/merge_requests) om een nieuwe oplossing voor te stellen wordt gewaardeerd.

Ook bij gebruik van een Referentie Implementatie en/of een van de benoemde oplossingen blijft het belangrijk om het cluster te valideren met de Haven Compliancy Checker. De specifieke inrichting bepaalt namelijk of het cluster echt compliant is.

&ensp;

---

*Gelukt? Tijd voor de [Haven Compliancy Checker](/docs/compliancy-checker)!*
